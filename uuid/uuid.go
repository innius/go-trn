package uuid

import "github.com/satori/go.uuid"

type UUID struct {
	uuid uuid.UUID
}

func NewV4() UUID {
	uuid, err := uuid.NewV4()
	if err != nil {
		println(err.Error())
	}
	return UUID{uuid}
}

func (u UUID) String() string {
	return u.uuid.String()
}
