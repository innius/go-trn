module bitbucket.org/innius/go-trn

go 1.13

require (
	github.com/pkg/errors v0.8.0
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/stretchr/testify v1.9.0
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
