package trn

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
	CompanyID      string `json:"companyid"`
	ResourceType string `json:"resourcetype"`
	ResourceID   string `

*/

func TestParseTrn(t *testing.T) {
	var tests = []struct {
		stringValue string
		trn         *TRN
	}{
		{"trn:123456:machine:234234234", &TRN{"123456", "machine", "234234234", nil}},
		{"trn:cid:machine:*", &TRN{"cid", "machine", "*", nil}},
		{"trn:cid:machine:123455/specs/voltage/220kv", &TRN{"cid", "machine", "123455", &Resource{"specs", "voltage/220kv"}}},
		{"trn:cid:machine:123455/specs", &TRN{"cid", "machine", "123455", &Resource{"specs", ""}}},
	}

	for _, test := range tests {
		action, err := Parse(test.stringValue)
		assert.Nil(t, err)
		assert.Equal(t, test.trn, action)
	}

}

func TestNewTrn(t *testing.T) {
	trn := New("plantage", Company, "1234")

	assert.Equal(t, "trn:plantage:company:1234", trn.String())

}

func TestOwns(t *testing.T) {
	trn := New("acme.com", Machine, "1234")

	owns, err := Owns("acme.com", trn.String())
	assert.Nil(t, err)
	assert.True(t, owns)

	owns, err = Owns("johndoe.com", trn.String())
	assert.Nil(t, err)
	assert.False(t, owns)
}

func TestParseTrnWithTrnSubResource(t *testing.T) {
	res := "trn:cid:machine:mid/parts/trn:cid:machine:pid"
	out, err := Parse(res)
	assert.Nil(t, err)
	assert.Equal(t, "cid", out.CompanyID)
	assert.Equal(t, "mid", out.ResourceID)
	assert.Equal(t, Machine.String(), "machine")
	assert.NotNil(t, out.SubResource)
	assert.Equal(t, "parts", out.SubResource.ResourceType)
	assert.Equal(t, "trn:cid:machine:pid", out.SubResource.ResourceID)
}

func TestParseTrnWithoutCompanyId(t *testing.T) {
	_, err := Parse("trn:foo:machine:")
	assert.NotNil(t, err)

	_, err = Parse("trn::machine:234234234")
	assert.NotNil(t, err)

}
