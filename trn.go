package trn

import (
	"fmt"
	"strings"
	"github.com/pkg/errors"
	"bitbucket.org/innius/go-trn/uuid"
)

type Resource struct {
	ResourceType string `json:"resourcetype"`
	ResourceID   string `json:"resourceid"`
}

type TRN struct {
	CompanyID    string `json:"companyid"`
	ResourceType string `json:"resourcetype"`
	ResourceID   string `json:"resourceid"`
	SubResource  *Resource
}

// InvalidFormatError indicates the format of a trn is not valid
type InvalidFormatError struct {
	error
}

// New returns a new instance of a trn based on specified values
func New(companyID string, resourceType ResourceType, resourceID string) *TRN {
	return &TRN{companyID, resourceType.String(), resourceID, nil}
}

func NewMachine(companyID string) *TRN {
	return New(companyID, Machine, uuid.NewV4().String())
}

// NewWithSubResource returns a new instance with a subresource
func NewWithSubResource(companyID string, resourceType ResourceType, resourceID, subResourceType, subResourceID string) *TRN {
	return &TRN{companyID, resourceType.String(), resourceID, &Resource{subResourceType, subResourceID}}
}

// Parse returns a *TRN based on a trn string
func Parse(trn string) (*TRN, error) {
	e := strings.SplitN(trn, ":", 4)
	if !strings.HasPrefix(trn, "trn:") || len(e) != 4 {
		return nil, &InvalidFormatError{fmt.Errorf("%v is not valid; format of a trn string is trn:<companyid>:<resourcetype>:<resourceid>", trn)}
	}
	if _, err := ParseType(e[2]); err != nil {
		return nil, err
	}
	if e[1] == "" {
		return nil, &InvalidFormatError{errors.New("Company id of a TRN can not be empty")}
	}
	if e[3] == "" {
		return nil, &InvalidFormatError{errors.New("Resource cannot be empty")}
	}
	rp := strings.SplitN(e[3], "/", 3)
	var r *Resource
	if len(rp) == 3 {
		r = &Resource{rp[1], rp[2]}
	} else if len(rp) == 2 {
		r = &Resource{rp[1], ""}
	}

	return &TRN{e[1], e[2], rp[0], r}, nil
}

func (r *Resource) String() string {
	result := r.ResourceType
	if r.ResourceID != "" {
		result += "/" + r.ResourceID
	}
	return result
}

func (trn *TRN) String() string {
	result := fmt.Sprintf("trn:%v:%v:%v", trn.CompanyID, trn.ResourceType, trn.ResourceID)
	if trn.SubResource != nil {
		result += "/" + trn.SubResource.String()
	}
	return result
}

func (trn *TRN) OwnedBy(companyid string) bool {
	return trn.CompanyID == companyid
}

/* Checks if the machine is owned by the company, by comparing the company part of the TRN
* @param companyid the UUID of the company
* @param machineid the TRN of the machine
 */
func Owns(companyid, machineid string) (bool, error) {
	trn, err := Parse(machineid)
	if err != nil {
		return false, err
	}
	return trn.OwnedBy(companyid), nil
}
