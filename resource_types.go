package trn

import "fmt"

// ResourceType is the enum value of a resource type
type ResourceType uint8

// ResourceTypes defines the resource types which can be used in trn's
const (
	Machine ResourceType = iota
	Company
	Person
	Group
)

func (t ResourceType) String() string {
	switch t {
	case Machine:
		return "machine"
	case Company:
		return "company"
	case Person:
		return "person"
	case Group:
		return "group"
	}
	return "unknown"
}

// ParseType takes a string resource type and returns the trn resource type constant.
func ParseType(t string) (ResourceType, error) {
	switch t {
	case "machine":
		return Machine, nil
	case "company":
		return Company, nil
	case "person":
		return Person, nil
	case "group":
		return Group, nil
	}

	var l ResourceType
	return l, fmt.Errorf("not a valid resource type: %q", t)
}
